SUBPTOOLS_SRCS=	subptools.c
SUBPTOOLS_OBJS=	${SUBPTOOLS_SRCS:.c=.o}

SUBP2PGM_SRCS=	subp2pgm.c \
		spudec.c \
		vobsub.c
SUBP2PGM_OBJS=	${SUBP2PGM_SRCS:.c=.o}

#CFLAGS+=		`pkg-config --cflags libxml-2.0`

SUBPTOOLS_LDFLAGS=	`pkg-config --libs libxml-2.0`

PROGS=	subp2pgm

all: ${PROGS}

subptools: ${SUBPTOOLS_OBJS}
	${CC} -o subptools ${SUBPTOOLS_OBJS} ${SUBPTOOLS_LDFLAGS} -lm

subp2pgm: ${SUBP2PGM_OBJS}
	${CC} -o subp2pgm ${SUBP2PGM_OBJS} -lm

clean:
	rm *.o
	rm ${PROGS}
